export class WudAbstractNote extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.attachShadow({ mode: "open" })

        const wudCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        wudCssLink.rel = "stylesheet"
        wudCssLink.href = "/css/whats-up-doc.css"

        const picoCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        picoCssLink.rel = "stylesheet"
        picoCssLink.href = "/css/pico.min.css"

        const aside = this.shadowRoot.appendChild(document.createElement("aside"))
        aside.setAttribute("class", "wud-note")

        this.addIcon(aside)

        const contents = aside.appendChild(document.createElement("div"))
        contents.appendChild(document.createElement("slot"))
    }

    addIcon(aside) { }
}
