import { WudAbstractNote } from "./wud-abstract-note.js";

class WudInfo extends WudAbstractNote {
    constructor() {
        super();
    }

    addIcon(aside) {
        const icon = aside.appendChild(document.createElement("img"))
        icon.src = "/assets/info.svg"
        icon.alt = "Note: "
        icon.title = "Note"
    }
}

(() => {
    customElements.define("wud-info", WudInfo)
})()
