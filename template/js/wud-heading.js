export class WudHeading extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.attachShadow({ mode: "open" })

        const wudCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        wudCssLink.rel = "stylesheet"
        wudCssLink.href = "/css/whats-up-doc.css"

        const picoCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        picoCssLink.rel = "stylesheet"
        picoCssLink.href = "/css/pico.min.css"

        const headingElement = this.getAttribute("el")
        const id = this.getAttribute("id")

        const h = this.shadowRoot.appendChild(document.createElement(headingElement))
        h.id = "id"

        const link = h.appendChild(document.createElement("a"))
        link.classList.add("secondary")
        link.href = `#${id}`
        link.appendChild(document.createElement("slot"))
    }
}

(() => {
    customElements.define("wud-heading", WudHeading)
})()
