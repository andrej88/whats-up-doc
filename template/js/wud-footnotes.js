const getRefId = (group, item) => `wud-fnote-ref-${group}-${item}`
const getEntryId = (group, item) => `wud-fnote-entry-${group}-${item}`

export class WudFnoteRef extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.attachShadow({ mode: "open" })

        const wudCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        wudCssLink.rel = "stylesheet"
        wudCssLink.href = "/css/whats-up-doc.css"

        const picoCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        picoCssLink.rel = "stylesheet"
        picoCssLink.href = "/css/pico.min.css"

        const group = this.getAttribute("group")
        const item = this.getAttribute("item")

        this.setAttribute("id", getRefId(group, item))

        const sup = this.shadowRoot.appendChild(document.createElement("sup"))
        const link = sup.appendChild(document.createElement("a"))
        link.href = `#${getEntryId(group, item)}`
        link.textContent = `${item}`
    }
}

export class WudFnoteGroup extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.attachShadow({ mode: "open" })

        const wudCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        wudCssLink.rel = "stylesheet"
        wudCssLink.href = "../css/whats-up-doc.css"

        const picoCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        picoCssLink.rel = "stylesheet"
        picoCssLink.href = "../css/pico.min.css"

        const ul = this.shadowRoot.appendChild(document.createElement("ul"))
        ul.appendChild(document.createElement("slot"))
        ul.classList.add("wud-fnote-group")
    }
}

export class WudFnoteEntry extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.attachShadow({ mode: "open" })

        const wudCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        wudCssLink.rel = "stylesheet"
        wudCssLink.href = "../css/whats-up-doc.css"

        const picoCssLink = this.shadowRoot.appendChild(document.createElement("link"))
        picoCssLink.rel = "stylesheet"
        picoCssLink.href = "../css/pico.min.css"

        const group = this.closest("wud-fnote-group").getAttribute("name")
        const item = this.getAttribute("item")

        this.setAttribute("id", getEntryId(group, item))

        const li = this.shadowRoot.appendChild(document.createElement("li"))
        li.classList.add("wud-fnote-entry")

        const numberAndArrow = li.appendChild(document.createElement("div"))
        const sup = numberAndArrow.appendChild(document.createElement("sup"))

        const link = sup.appendChild(document.createElement("a"))
        link.href = `#${getRefId(group, item)}`
        link.textContent = `↑`

        sup.appendChild(document.createTextNode(` ${item} `))

        const contentDiv = li.appendChild(document.createElement("div"))
        contentDiv.appendChild(document.createElement("slot"))
    }
}

(() => {
    customElements.define("wud-fnote-ref", WudFnoteRef)
    customElements.define("wud-fnote-group", WudFnoteGroup)
    customElements.define("wud-fnote-entry", WudFnoteEntry)
})()
