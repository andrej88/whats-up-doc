import pages from "../data/pages.json" assert { type: "json" }


function addBranch({ name, url, children }, parentElement) {

    const listElement = parentElement.appendChild(document.createElement("li"))

    if (children && children.length > 0) {
        const details = listElement.appendChild(document.createElement("details"))

        const summary = details.appendChild(document.createElement("summary"))

        addBranchRoot({ name, url }, summary)

        const subList = details.appendChild(document.createElement("ul"))
        children.forEach((entry) => {
            addBranch(entry, subList)
        })
    } else {
        addBranchRoot({ name, url }, listElement)
    }
}

function addBranchRoot({ name, url }, parentElement) {
    let textNode

    if (window.location.pathname.includes(url)) {
        textNode = document.createElement("strong")
        textNode.textContent = name
    } else {
        textNode = document.createTextNode(name)
    }

    if (url) {
        const link = parentElement.appendChild(document.createElement("a"))
        link.href = url
        link.appendChild(textNode)
    } else {
        parentElement.appendChild(textNode)
    }
}

(() => {
    const navPanel = document.getElementById("page-nav")
    const list = navPanel.appendChild(document.createElement("ul"))

    pages.forEach((entry) => {
        addBranch(entry, list)
    });

    const thisPageLink = document.querySelector(`a[href="${window.location.pathname}"]`)
    let nextDetailsParent = thisPageLink.closest("details")
    while (nextDetailsParent) {
        nextDetailsParent.open = true
        nextDetailsParent = nextDetailsParent.parentElement?.closest("details")
    }
})()
