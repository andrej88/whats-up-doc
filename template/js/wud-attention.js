import { WudAbstractNote } from "./wud-abstract-note.js";

class WudAttention extends WudAbstractNote {
    constructor() {
        super();
    }

    addIcon(aside) {
        const icon = aside.appendChild(document.createElement("img"))
        icon.src = "/assets/attention.svg"
        icon.alt = "Attention: "
        icon.title = "Attention"
    }
}

(() => {
    customElements.define("wud-attention", WudAttention)
})()
